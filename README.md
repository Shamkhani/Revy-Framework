# چارچوبی برای پیاده سازی منطق بازی در Unity3D
 
## <a id="introduction"></a> مقدمه
این چهارچوب کمک می کند کنترل بیشتری بر روی روند اجرای منطق بازی داشته باشید و همچنین با پشتیبانی از Dependency Injection کمک می کند تا وابستگی کلاس ها با یکدیگر را به حداقل برسانید همچنین با مطرح کردن مفاهیم Game System و Subsystem کدهای بازی را به صورت منطقی دسته بندی نمایید که به قابلیت استفاده مجدد از کدها کمک می نماید.
 
بر روی سکوهای زیر امتحان گردیده است:
 
* پی سی/ مک / لینوکس
* اندروید
 
پشتیبانی از IL2CPP.
 
## ویژگی ها
+ پشتیبانی از Injection برای  Field و Property با استفاده از Attribute بر روی کلاس هایی که از FComponent به ارث می برند
+ پشتیبانی از Injection  توسط Service Locator
+ پشتیبانی از Injection برای اشیایی که به صورت Runtime ساخته می شوند
+ پشتیبانی کامل از Async/Await
+ توابع کمکی برای تبدیل Coroutine به Task
+ پشتیبانی از مفهوم Game Mode به وسیله ارتباط دادن (Bind) کلاس های Game System  به GameManager
+ افزایش کارایی با جایگزینی متدهای Update در MonoBehaviour با متدهایی که از طریق چارچوب فراخوانی می گردند [(1)](https://blogs.unity3d.com/2015/12/23/1k-update-calls/)
+ استفاده از Assembly Definition File در جهت افزایش سرعت کامپایل
 
## مشکل و راه حل
### مشکل
چهارچوب پیش فرض کدنویسی در یونیتی زمانی که پروژه متوسط یا بزرگ باشد و همچنین نگهداری کد در این چارچوب چندان مناسب نیست. همانطور که مشخص است این شرایط برای پروژه های کوچک که شامل یک یا دو برنامه نویس است چندان صدق نمی کند و باید گفت برای چنین شرایطی یونیتی به اندازه کافی مناسب است و حتی اگر از چارچوب های دیگر استفاده نمایید باعث می شود زمان پیاده سازی پروژه و همچنین پیچیدگی بی موردی به آن اضافه گردد.

چهارچوب کدنویسی یونیتی بر اساس مفهوم موجودیت هایی(Entity) که بر اساس مولفه ها(Component) شکل می گیرند ساخته شده است. البته باید گفت این طراحی در زمان نوشتن این مستند در حال تغییر به سیستمی موسوم به Entity-Component-System است که با رویکردی داده گرا(Data Oriented) روشی کاملا متفاوت را مطرح می کند(برای اطلاعات بیشتر می توانید به [این مقاله](https://virgool.io/@ideenmolavi/introduction-to-ecs-in-unity-pwtxq8cidakz) رجوع کنید).

طراحی مبتنی بر موجودیت و مولفه نکات مثبت زیاد دارد که می توان به تشویق برنامه نویس برای رعایت الگوی Composition over Inheritance و Single Responsibility بودن کلاس ها اشاره کرد همچنین به ماژولار بودن پیاده سازی منطق نیز کمک می کند.
در یونیتی به موجودیت ها GameObject گفته می شود و مولفه ها براساس MonoBehaviour ساخته می شوند. مولفه ها به موجودیت رفتار ها را اضافه می کنند.
 
مولفه ها می توانند به مولفه های دیگری که در یک موجودیت هستند توسط تابع GetComponent دسترسی پیدا کنند ولی نمی توانند به مولفه های دیگر که در موجودیت های دیگر هستند دسترسی پیدا کنند. یونیتی روش چندان مناسبی را برای این کار  پیشنهاد نمی دهد.
 
در یونیتی میتوان برای دستیابی به مولفه های دیگر سه روش زیر را استفاده کرد:
+ استفاده از GameObject.Find
 
بدون در نظر گرفتن این که این تابع بسیار کند است پیدا کردن یک موجودیت به وسیله نام حتی برای یک پروژه کوچک نیز کار مناسبی نیست در نظر بگیرید بعدا نیاز به تغییر نام این موجودیت را داشته
باشید نیازمند آن هستید تا تمامی جاهایی ک به این موجودیت دسترسی داشته باشند را نیز تغییر
بدهید همچنین باعث می شود مشکلات احتمالی در زمان کامپایل مشخص نشده و مجبور باشید در
زمان اجرا به این مشکلات رسیدگی کنید.
 
+ استفاده از Object.FindObjectOfType
 
این تابع اولین شی فعال از نوع مشخص شده را برمیگرداند مشکل اصلی این تابع غیر ممکن بودن استفاده از Interface برای یافتن یک مولفه است. Interface یکی از مهمترین مفاهیم برای طراحی یک کد خوب است.
 
+ الگوی Singleton
 
باید گفت این الگو برای پروژه های کوچک مناسب است ولی به طوری کلی مفهوم برنامه نویسی برای Interface را رعایت نمی کند و باعث به وجود آمدن وابستگی بالا در کلاس های می شود.
 
### راه حل
راه حلی که این چارچوب برای ایجاد ارتباط بین مولفه ها و همچنین رعایت اصل وابستگی بین کلاس ها پیشنهاد می دهد استفاده از DI به وسیله Service Locator می باشد. نمی توان گفت استفاده از Service Locator بهترین پیاده سازی DI است ولی تعادل مناسبی بین سادگی و رعایت اصول طراحی نرم افزار را مورد استفاده قرار می دهد.
همچنین این چارچوب با استفاده از کلاس هایی مانند FComponent و مفاهیمی مانند Game System و Subsystem تا حدود زیاد استفاده مستقیم از Service Locator را محدود کرده است و بیشتر Injection ها به صورت خودکار اتفاق می افتد.

# مستند
این مستند در جهت راهنمایی کاربر برای شروع استفاده از چارچوب طراحی گردیده است و رویکردی قدم به قدم در جهت آموزش مفاهیم، روش کار و اصطلاحات به کار رفته در چارچوب را دارد.

## فهرست مطالب
+ مقدمه
+ [مروری بر نحوه کار چارچوب](#مروری-بر-نحوه-کار-چارچوب)
+ مفاهیم کلیدی
+ [روند مقداردهی اولیه](#روند-مقداردهی-اولیه)
+ [روند بروز رسانی](#روند-بروز-رسانی)
+ [کلاس FComponent] (#کلاس-FComponent)
+ [کلاس FClass] (#کلاس-FClass)

+ [مفهوم سیستم فرعی](#مفهوم-سیستم-فرعی)
+  [مفهوم سیستم بازی](#مفهوم-سیستم-بازی)
+ [مفهوم شی ایستا و گذرا](#مفهوم-شی-ایستا-و-گذرا)
+ [روند ثبت اشیا در چارچوب](#روند-ثبت-اشیا-در-چارچوب)

+ قراردادها
+ [اصول نام گذاری و کدنویسی استاندارد](#اصول-نام-گذاری-و-کدنویسی-استاندارد)
+ [منطقه بندی](#منطقه-بندی)
+ تنظیمات چارچوب	
+ نمونه های آموزشی
	+ [نصب چارچوب](#نصب-چارچوب)
	+ [ساخت کلاس مدیریت کننده بازی](#ساخت-کلاس-مدیریت-کننده-بازی)
+ [ساخت کلاس مدیر](#ساخت-کلاس-مدیر)
+ [فرآیند مقدار دهی اولیه و بروزرسانی](#فرآیند-مقدار-دهی-اولیه-و-بروزرسانی)
+ [بارگذاری در زمان اجرا](#بارگذاری-در-زمان-اجرا)
+ تنظیم چارچوب برای بازی
	+ سیستم فرعی Pooling
	+ سیستم فرعی CLog
	+ سیستم فرعی State


## مروری بر نحوه کار چارچوب
کلاس MFramework هسته اصلی چارچوب است و تمامی امور مربوط به چارچوب از این کلاس آغاز می گردد.<br>
این کلاس تنها کلاسی است در چارچوب که مستقیما از Monobehaviour به ارث می برد.<br>
توسط متد [Startup](https://gitlab.com/ideen/Revy-Framework/wikis/startup) چارچوب شروع به آماده کردن نیازمندی هایش می نماید. در ابتدا GameObject های اصلی با مانند Transient و Persistent ساخته میشوند و سپس نمونه ای از کلاس MFramework ساخته شده و به Persistent اضافه می گردد. توسط این روش دیگر نیازی نیست که چارچوب را از قبل در صحنه قرار دهیم، این امر به صورت خودکار انجام شده و وابسته به ساختار صحنه نمی باشد.

زمانی که از چارچوب استفاده می کنید دیگر نباید از کلاس هایی که از Monobehaviour به ارث می برند استفاده کنید بلکه باید از کلاس جایگزینی به نام FComponent استفاده نمایید. بدین وسیله شی ساخته شده از این کلاس به صورت خودکار در چارچوب ثبت شده و می تواند از امکانات آن استفاده کند.

چارچوب برای اینکه وابسته به ساختار صحنه نباشد تمامی کلاس های مهم را به صورت خودکار می سازد و برای این کار از مفاهیم GameManager و GameSystem استفاده می کند. بر طبق این ساختار هر بازی نیازمند یک کلاس از نوع GameManager است این کلاس مدیر کلی بازی است و بقیه کلاس های مدیریت کننده توسط ارتباط با این کلاس به صورت خودکار ساخته می شود.

از این رو تمامی اشیایی که دارای رفتار هستند در نظر گرفته می شود که به وسیله مفاهیم GameManager و GameSystem در زمان راه اندازی چارچوب و به صورت خودکار ساخته شوند و نه اینکه در صحنه از قبل حضور داشته باشند. با این حال اشیایی که در صحنه حضور دارند نیز به صورت خودکار در چارچوب ثبت خواهند شد.

## روند مقداردهی اولیه
مقدار دهی اولیه در چارچوب برای کلاس هایی که واسط IInitializable را پیاده سازی کرده اند امکان پذیر است. FComponent به طور پیش فرض این واسط را پیاده کرده و کلاسهایی که از آن به ارث می برند به طور خودکار وارد روند مقدار دهی اولیه می شوند.

این روند با فراخوانی سه متد از واسط IInitializable فرآیند را انجام می دهد این سه متد عبارت اند از:
+ متد PreInitialization  
اولین متدی است که بر روی تمامی اشیا با قابلیت مقدار دهی اولیه توسط چارچوب فراخوانی می گردد. از این متد بیشتر برای مشخص کردن تنظیمات اولیه شی در چارچوب استفاده قرار می گیرد.

+ متد Initialize  
پس از تمامی PreInitialization ها فراخوانی می گردد. این متد مانند متد  Start در MonoBehaviour عمل میکند.
 در این متد می توانید منابعی که کلاس احتیاج دارد بارگذاری کنید و مقدار دهی های اولیه برای کلاس را انجام دهید، همچنین این متد به صورت Async پیاده سازی شده است.

+ متد BeginPlay  
پس از تمامی Initialize ها فراخوانی می شود و به صورت Async پیاده سازی شده است. در این تابع به صورت منطقی تمامی اشیای دیگر مقدار دهی اولیه شده اند و آماده سرویس دهی می باشند از این رو در این متد می توانید به صورت امن به کلاس های دیگر دسترسی پیدا کنید.<br> 
پس از اتمام فراخوانی تمامی متد های BeginPlay چارچوب آغاز به فراخوانی توابع به روز رسانی مانند Tick و LateTick را می نماید.

### فرآیند Shutdown و Pause
+ متد Shutdown  
پس از آن که شی نابود می گردد یا زمانی که چارچوب اقدام به توقف روند کاری خود می کند متد Shutdown بر روی تمامی اشیا ای که واسط IInitializable را پیاده کرده اند فراخوانی می گردد  
+ متد های OnPause - OnResume  
زمانی که چارچوب توسط متد MFramework.Pause به حالت Pause می رود تمامی اشیا ای که واسط IInitializable را پیاده کرده اند تابع OnPause شان فراخوانی می گردد و در صورت ادامه متد OnResume برای آن ها فراخوانی می شود.

## روند بروز رسانی
تمامی کلاس هایی که واسط ITickable را پیاده سازی کرده و همچنین در چارچوب ثبت شده باشند متدهای بروز رسانی برایشان فراخوانی می شود. این متد ها عبارت اند از:
+ متد Tick  
هر فریم فراخوانی می شود و فاصله زمانی بین اجرای هر Tick برابر است با Time.deltaTime.

+ متد LateTick
پس از فراخوانی تمامی  Tick ها فراخوانی می شود.

+ متد FixedTick
مانند FixedUpdate در فواصل زمانی مشخص شده در تنظیمات پروژه فراخوانی می گردد.

## کلاس FComponent
این کلاس نسخه هماهنگ با چارچوب MonoBehaviour است و همچنین مفاهیم Subsystem و GameSystem بر پایه این 
کلاس طراحی گشته اند.<br>
کلاس هایی که از این کلاس به ارث می برند به صورت خودکار در چارچوب ثبت می شوند. <br>
این کلاس واسط های IInitializable و ITickable را پیاده کرده از این رو فرآیند مقداردهی اولیه و بروز رسانی پس از آن که نمونه ای از این کلاس در چارچوب ثبت شد آغاز می گردد.

## کلاس FClass
این کلاس نسخه هماهنگ با چارچوب از یک کلاس ساده است. زمانی که نمی خواهید اسکریپتی را به یک GameObject اضافه کنید ولی می خواهید قابلیت های مقدار دهی اولیه و بروز رسانی بهره مند بشوید بجای ارث بری از FComponent از FClass به ارث می برید. FClass تمامی امکاناتی را که چارچوب در اختیار کلاس FComponent قرار میدهد دارا می باشد و از کلاس FComponent سبک تر است.

نکته مهمی که باید توجه داشته باشید این است که پس از ساخت نمونه ای از این نوع کلاس، چارچوب همواره ارجاعی(Reference) از این نوع کلاس را در خود نگهداری می کند در این صورت هیچ وقت نمونه توسط GC از حافظه پاک نمی شود. یا به عبارت دیگر هیچ وقت تابع Destructor برای نمونه فراخوانی نمی شود.

 برای حل این مشکل تابع Unregister در FClass قرار داده شده است تا زمانی که دیگر نیازی به نمونه نبود آن را از چارچوب حذف نمایید بدین صورت GC می تواند آن را از حافظه پاک نماید.

## مفهوم سیستم فرعی
توسط این مفهوم چارچوب بستری را فراهم می آورد برای جدا کردن سیستم های مربوط به بازی از سیستم کلی (Generic) و عمومی.

 مثلا یک سیستم ذخیره و بازیابی داده قابلیت آن را دارد که به صورتی نوشته شود تا در بازی های مختلف مورد استفاده قرار بگیرد از این رو در نوشتن آن نیازمند آن هستیم که وابستگی کد ها به بازی را به صفر برسانیم و سیستمی کلی و غیر وابسته طراحی کنیم.<br>
چارچوب در زمان راه اندازی خود تمامی کلاس هایی که از نوع Subsystem هستند را در اسمبلی های موجود پیدا می کند و به صورت خودکار نمونه ای از آن ها را می سازد و به شی Persistent اضافه می کند، از این رو اشیای Subsystem تا زمانی
که برنامه بسته نشود در تمامی صحنه ها در دسترس هستند.

فرآیند مقدار دهی اولیه و همچنین بروز رسانی Subsystem ها قبل از Game System ها اتفاق می افتد بدین وسیله همیشه Subsystem ها در زمان اجرای بازی آماده سرویس می باشند.

## مفهوم سیستم بازی
اشیا Game System، مدیریت کننده ها اصلی بازی هستند. مثلا زمانی که می خواهیم یک مدیریت کننده مرحله (Level Manager) طراحی نماییم بهترین گزینه طراحی کلاسی است که ازنوع Game System باشد.
انواع Game System به صورت خودکار توسط چارچوب ساخت می شوند و تا زمانی که تمامی Game System ها فرآیند مقدار دهی خود را طی نکرده باشند بازی شروع نمی شود.

## مفهوم شی ایستا و گذرا
### شی ایستا
شی ایستا GameObject است که از ابتدای بارگذاری بازی و تا زمانی که بازی بسته نشده است در حافظه حضور دارد.
از این طریق تمامی مولفه هایی که به این شی اضافه می گردند نیز همیشه در دسترس هستند.
سیستم های بازی و سیستم های فرعی به صورت خودکار زیرمجموعه ای از این شی هستند و از این طریق اطمینان داده می شود که همیشه در دسترس باشند.
کلاس FComponent شامل متدهایی برای نمونه سازی(Instantiate) می باشد که می توانید به کمک این متدها شی را گذرا و یا ایستا نمایید.
### شی گذرا
شی گذرا در زمان بارگذاری صحنه جدید از حافظه پاک می شود و تمامی نمونه های موجود از آن برابر با null می گردد.
اشیایی که وابسته به صحنه هستند و نیاز نیست در کل زمان اجرای برنامه در دسترس باشند بهتر است از این نوع باشند.
## روند ثبت اشیا در چارچوب
اشیایی که از نوع مولفه های چارچوب هستند(FComponent) در زمان نمونه سازی به صورت خودکار در چارچوب ثبت میگردند و زمانی که از صحنه حذف می شوند(مثلا با دستور Destroy) به طور خودکار از چارچوب نیز حذف می گردند تا هم از چارچوب حذف گردند و هم GC بتواند آن ها را از حافظه حذف کند.

اشیای FCLass در زمان نمونه سازی به صورت خودکار در چارچوب ثبت میشوند ولی زمان که از محدوده خارج شوند به علت اینکه چارچوب نمونه از آنها در خود نگهداری می کند هیچگاه تابع مخرب(Destructor) برای آن ها فراخوانی نمیشود از این رو چارچوب راهی برای حذف کردن این اشیا به صورت خودکار ندارد بدین علت این نوع اشیا باید به صورتی دستی با فراخوانی تابع FCLass.Unregister خود را از چارچوب حذف نمایند.

## اصول نام گذاری و کدنویسی استاندارد
| Object Name    | Notation   | Length | Plural | Prefix | Suffix | Abbreviation | Char Mask          |Underscores  |
|:--------------------------|:-----------|-------:|:-------|:-------|:-------|:-------------|:-------------------|:------------|
| Class name                | PascalCase |    128 | No     | No     | Yes    | No           | [A-z][0-9]         | No          |
| Constructor name          | PascalCase |    128 | No     | No     | Yes    | No           | [A-z][0-9]         | No          |
| Method name               | PascalCase |    128 | Yes    | No     | No     | No           | [A-z][0-9]         | No          |
| Private/Protected Method name       | camelCase |    128 | No    | No     | No     | No           | [A-z][0-9]         | Yes          |
| Method arguments          | camelCase  |    128 | Yes    | No     | No     | Yes          | [A-z][0-9]         | No          |
| Local variables           | camelCase  |    50  | Yes    | No     | No     | Yes          | [A-z][0-9]         | No          |
| Constants name            | PascalCase |    50  | No     | No     | No     | No           | [A-z][0-9]         | No          |
| Field name                | camelCase  |    50  | Yes    | No     | No     | Yes          | [A-z][0-9]         | Yes         |
| Properties name           | PascalCase |    50  | Yes    | No     | No     | Yes          | [A-z][0-9]         | No          |
| Delegate name             | PascalCase |    128 | No     | No     | Yes    | Yes          | [A-z]              | No          |
| Enum type name            | PascalCase |    128 | Yes    | No     | No     | No           | [A-z]              | No          |
  #### 1. از Pascal Casing برای کلاس ها و نام متد ها استفاده نمایید:

```csharp
public class Debug
{
   	public void DisplayLog()
   	{
     	//...
   	}
 }
```

### 2. از Camel Casing برای آرگومان متدها و متغیر های محلی استفاده نمایید:

```csharp
public class UserLog
{
public void Add(LogEvent logEvent)
    	{
    		int itemCount = logEvent.Items.Count;
    		// ...
	}
}
```
### 3. از علامت گذاری به سبک Hungarian و یا هر نوع قرار دادن شناسه نوع خودداری نمایید:

```csharp
    // Correct
    int counter;
    string name;    
    // Avoid
    int iCounter;
    string strName;
```
### 4. برای ثابت ها و متغیر های فقط خواندنی از حروف بزرگ استفاده ننمایید:

```csharp
    // Correct
    public static const string ShippingType = "DropShip";    
    // Avoid
    public static const string SHIPPINGTYPE = "DropShip";
```

### 5. از نام های با معنی برای نام گذاری متغیر ها استفاده نمایید:

```csharp
    var seattleCustomers = from cust in customers
    where cust.City == "Seattle" 
    select cust.Name;
```

### 6. برای نام گذاری از مخفف ها استفاده ننمایید:

```csharp    
	// Correct
    UserGroup userGroup;
    Assignment employeeAssignment;     
    // Avoid
    UserGroup usrGrp;
    Assignment empAssignment; 
    // Exceptions
    CustomerId customerId;
    XmlDocument xmlDocument;
    FtpHelper ftpHelper;
    UriPart uriPart;
```
### 7. از Pascal Casing برای مخفف های سه کاکتری و بیشتر استفاده نمایید:

```csharp  
  	HtmlHelper htmlHelper;
    FtpTransfer ftpTransfer;
    UIControl uiControl;
```

### 8. از Underscore برای نام گذاری استفاده ننمایید بجز در متغیر ها و متدهای خصوصی :

```csharp 
    // Correct
    public DateTime clientAppointment;
    public TimeSpan timeLeft;    
    // Avoid
    public DateTime client_Appointment;
    public TimeSpan time_Left; 
    // Exception (Class field)
    private DateTime _registrationDate;
    private void _displayLog();
```

#### 9. از نام های پیشفرض برای نوع ها استفاده کنید بجای نام هایی که سیستم استفاده می کند:

```csharp
 	// Correct
    string firstName;
    int lastIndex;
    bool isSaved;
    // Avoid
    String firstName;
    Int32 lastIndex;
    Boolean isSaved;
```


#### 10. از نام گذاری ضمنی برای متغیر های محلی استفاده کنید بجز انواع اصلی (int,string,double)

```csharp 
    var stream = File.Create(path);
    var customers = new Dictionary();
    // Exceptions
    int index = 100;
    string timeSheet;
    bool isCompleted;
```


#### 11. برای نام گذاری کلاس ها از اسم و یا عبارت اسمی استفاده نمایید.

```csharp 
    public class Employee
    {
    }
    public class BusinessLocation
    {
    }
    public class DocumentCollection
    {
    }
```

#### 12.نام واسط ها با پیشوند I شروع شود. نام واسط باید اسم یا عبارت اسمی و یا صفت باشد:

```csharp     
public interface IShape
{
}
public interface IShapeCollection
{
}
public interface IGroupable
{
}
```


#### 13. نام فایل کد منبع را بر اساس کلاس اصلی ای که در آن تعریف شده است نام گذاری نمایید:

```csharp 
    // Located in Task.cs
    public partial class Task
    {
		//...
    }
```


#### 14. از namespace ها برای سازماندهی بهتر کد ها استفاده نمایید:
```csharp 
    // Examples
    namespace Company.Product.Module.SubModule
    namespace Product.Module.Component
    namespace Product.Layer.Module.Group
```


#### 15. آکولاد ها را به صورت عمودی با یکدیگر تراز نمایید:
```csharp 
    // Correct
    class Program
    {
      static void Main(string[] args)
      {
      }
    }
```



#### 16. تمامی متغیر ها را در بالای کلاس تعریف نمایید و متغیر های ایستا را بالاتر از بقیه متغیر ها:

```csharp 
    // Correct
    public class Account
    {
      public static string BankName;
      public static decimal Reserves;      
      public string Number {get; set;}
      public DateTime DateOpened {get; set;}
      public DateTime DateClosed {get; set;}
      public decimal Balance {get; set;}     
      // Constructor
      public Account()
      {
           // ...
      }
     }
```


#### 17. از نام مفرد برای enum ها استفاده نمایید بجز enum هایی از نوع bit field:

```csharp 
    // Correct
    public enum Color
    {
        Red,
        Green,
        Blue,
        Yellow,
        Magenta,
        Cyan
    } 
    // Exception
    [Flags]
    public enum Dockings
    {
       None = 0,
       Top = 1,
       Right = 2, 
       Bottom = 4,
       Left = 8
    }
```


#### 18. نوع enum را به صورت صریح مشخص ننمایید:

```csharp 
    // Don't
    public enum Direction : long
    {
      North = 1,
      East = 2,
      South = 3,
      West = 4
    } 
    // Correct
    public enum Direction
    {
       North,
       East,
       South,
       West
    }
```


#### 19. از پیشوند و پسوند برای نامگذاری enum استفاده ننمایید:

```csharp     
// Don't
    public enum CoinEnum
    {
      Penny,
      Nickel,
      Dime,
      Quarter,
      Dollar
    } 
    // Correct
    public enum Coin
    {
       Penny,
       Nickel,
       Dime,
       Quarter,
       Dollar
    }
```


#### 20. از کلمه Flag یا Flags به عنوان پیشوند و یا پسوند برای enum استفاده نکنید:

```csharp 
    //Don't
    [Flags]
    public enum DockingsFlags
    {
       None = 0,
      Top = 1,
      Right = 2, 
      Bottom = 4,
       Left = 8
    }
    //Correct
    [Flags]
    public enum Dockings
    {
       None = 0,
       Top = 1,
       Right = 2, 
       Bottom = 4,
       Left = 8
    }	  
``` 

#### 21. نام پارامتر توابع را به صورتی که فقط در حروف کوچک و بزرگ تفاوت داشته باشند انتخاب ننمایید:

```csharp 
    // Avoid
    private void MyFunction(string name, string Name)
```


#### 22. از پسوند Exception برای نام گذاری کلاس هایی که Exception را پیاده سازی کرده اند استفاده نمایید:

```csharp 
    // Correct
    public void BarcodeReadException : System.Exception 
```
### 23. از string interpolation برای اتصال رشته ها استفاده کنید:

```csharp 
string displayName = $"{nameList[n].LastName}, {nameList[n].FirstName}";
```

### 24. از انواع Unsigned تا جایی که ممکن است استفاده ننمایید در این صورت برای ارتباط با کدهای دیگر کمتر به مشکل بر خواهید خورد.

### 25. دلیگیت ها را در ابتدای کلاس تعریف نمایید:
```csharp
// First, in class Program, define the delegate type and a method that  
// has a matching signature.

// Define the type.
public delegate void Del(string message);

// Define a method that has a matching signature.
public static void DelMethod(string str)
{
    Console.WriteLine("DelMethod argument: {0}", str);
}
```



## منطقه بندی
هر کد بر حسب نیاز می تواند مناطق (Region) زیر را داشته باشد همچنین ترتیب استفاده نیز بهتر است به همین صورت باشد

```csharp 
//Each static items needs to be in their own region.  
#region Fields  
//Protected fields  
//Then private Fields  
#endregion  

#region Properties  
//Public variables or public properties  
#endregion  

#region Public Methods  
#endregion  

#region Interface Implementation  
//This is optional but it's better to   
//Implement each interface in separate region  
#endregion  

#region Helpers  
//Protected methods  
//Then private methods  
#endregion  
```

## نصب چارچوب
نحوه نصب چارچوب در این آموزش شرح داده شده است.

![آموزش نصب چارچوب](https://hw19.cdn.asset.aparat.com/aparat-video/3ba841affb544e3fd0b9adbefce6491012708040-720p__89600.mp4)

## ساخت یک سیستم فرعی
در این آموزش نحوه ساخت یک سیستم فرعی شرح داده می شود.
![آموزش ساخت یک سیستم فرعی](https://hw19.cdn.asset.aparat.com/aparat-video/014303cbaa4eb81885ab508aae511d7312631371-720p__34819.mp4)

## ساخت کلاس مدیریت کننده بازی
در این آموزش نحوه ساخت کلاس مدیریت کننده بازی شرح داده می شود.
![آموزش ساخت کلاس مدیریت کننده بازی](https://hw15.cdn.asset.aparat.com/aparat-video/f9db233b24ec662e234a08b8b02d86a712146985-666p__79205.mp4)


## ساخت کلاس مدیر
در این آموزش نحوه ساخت کلاس مدیر شرح داده شده است.

![آموزش ساخت کلاس مدیریت](https://hw6.cdn.asset.aparat.com/aparat-video/4eb77aae1cda5c2752736bb572e32e1112160754-720p__22028.mp4)

## فرآیند مقدار دهی اولیه و بروزرسانی

![فرآیند مقدار دهی اولیه و بروزرسانی](https://hw13.cdn.asset.aparat.com/aparat-video/159458e4741a4a25f75f3475a35a946a12163475-720p__23828.mp4)


## بارگذاری در زمان اجرا
در این آموزش نحوه استفاده از سیستم فرعی مدیر منابع را بررسی می کنیم.

![بارگذاری در زمان اجرا](https://hw15.cdn.asset.aparat.com/aparat-video/b95ce62d951db7b344e12a0dc9df03db12164886-720p__87554.mp4)

## آموزش نحوه استفاده از Terminal و Lunar Mobile Console

![نحوه استفاده از Terminal و Lunar Mobile Console](https://hw4.cdn.asset.aparat.com/aparat-video/fc0f524472596d133981fa1fbd5246ca12631458-720p__56678.mp4)


















