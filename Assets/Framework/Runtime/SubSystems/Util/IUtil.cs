using UnityEngine;
using Revy.Framework;

namespace Revy.Framework
{
    public interface IUtil
    {
        ISchedule Schedule { get; }
    }
}