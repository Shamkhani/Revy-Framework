using UnityEngine;
using Revy.Framework;

namespace Revy.Framework
{
    public sealed class Util : ISubsystem, IUtil
    {
        #region Fields

        private static ISchedule _schedule = new Schedule();

        #endregion Fields

        #region Properties

        public System.Type ServiceType => typeof(IUtil);

        public ISchedule Schedule => _schedule;

        #endregion Properties

        #region Constructor & Destructor

        public Util()
        {
            MFramework.Register(this);
        }

        ~Util()
        {
            Debug.Log("Util Destructor has been invoked");
            MFramework.UnRegister(this);
        }

        #endregion Constructor & Destructor

        #region Public Interface

        void IDispose.Dispose()
        {
            _schedule = null;
        }

        #endregion Public Methods

        #region Helpers     

        #endregion Helpers
    }
}