﻿/**
* Author: Mohammad Hasan Bigdeli
* CreationDate: 11 / 21 / 2017
* Description: base class for all game system interfaces.
*/

namespace Revy.Framework
{
    public interface IGameSystem : IDIRegister, IDispose
    {
    }
}