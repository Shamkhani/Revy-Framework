using Revy.Framework;
using System.Threading.Tasks;
using UnityEngine;

namespace MyNamespace
{
    public class NewFComponent : FComponent, INewFComponent, ITick , IExecutionOrder
    {  
//        public bool HasInitialized { get; set; }
//
//        //GameObject IPersistable.ObjectToPersist => gameObject;
//
//        //public GameObject ObjectToTransient => gameObject;
//
//        protected override void Awake()
//        {
//            base.Awake();
//            Debug.Log($"{GetType().Name} Awake Invoked. {Time.frameCount}");
//        }
//
//        protected override void OnEnable()
//        {
//            base.OnEnable();
//            Debug.Log($"{GetType().Name} OnEnable Invoked. {Time.frameCount}");
//        }
//
//        protected override void OnDisable()
//        {
//            base.OnDisable();
//            Debug.Log($"{GetType().Name} OnDisable Invoked. {Time.frameCount}");
//        }
//
//        public void Initialize()
//        {
//            Debug.Log($"{GetType().Name}  Initialize Invoked. {Time.frameCount}");
//            gameObject.AddComponent<NewFComponent1>();
//        }
//
//        public Task BeginPlay()
//        {
//            Debug.Log($"{GetType().Name}  BeginPlay Invoked. {Time.frameCount}");
//            return Task.CompletedTask;
//        }

        public void Tick()
        {
//            Debug.Log($" {GetType().Name} - {Time.frameCount}");
        }

        public bool IsActive { get; set; }
        public int ExecutionOrder => 10;
    }
}